/* restrict.c: Auxiliary functions needed to restrict the operation of red */
/*-
 * Copyright (C) 2022 KomasanZura
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <limits.h>
#include <unistd.h>
#include <errno.h>

#include "ed.h"

int
invoked_red(char *argv0)
{
	char *progname;
	progname = strrchr(argv0, '/');
	if (progname) {
		progname++;
	} else {
		progname = argv0;
	}
	return *progname == 'r';
}

int
is_legal_filename(char *s, int hasnl)
{
	static char *cwd = NULL;
	static char *path = NULL;
	static char *buf = NULL;
	static int cwdlen = 0;
	char *bufp;
	if (restricted) {
		if (*s == '!') {
			seterrmsg("shell access restricted");
			return 0;
		}
		if (!cwdlen) {
			SPL1();
			if (!cwd) cwd = malloc(PATH_MAX);
			if (!path) path = malloc(PATH_MAX);
			if (!buf) buf = malloc(PATH_MAX);
			SPL0();
			if (!getcwd(cwd, PATH_MAX)) {
				seterrmsg(strerror(errno));
				return 0;
			}
			cwdlen = strlen(cwd);
		}
		if (cwdlen == 1) {
			return 1;
		}
		for (bufp = buf; *s; bufp++, s++) {
			if (hasnl && *s == '\n') {
				break;
			}
			if (*s && (bufp - buf >= PATH_MAX - 1)) {
				seterrmsg("filename too long");
				return 0;
			}
			*bufp = *s;
		}
		*bufp = 0;
		if (!realpath(buf, path)) {
			if (errno == ENOENT) {
				while (*bufp != '/' && bufp > buf) {
					bufp--;
				}
				if (bufp == buf) {
					return 1;
				}
				*bufp = 0;
				if (!realpath(buf, path)) {
					seterrmsg(strerror(errno));
					return 0;
				}
			} else {
				seterrmsg(strerror(errno));
				return 0;
			}
		}
		if (strncmp(path, cwd, cwdlen) == 0) {
			return 1;
		} else {
			seterrmsg("restricted to current directory");
			return 0;
		}
	}
	return 1;
}
