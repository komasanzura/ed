/* history.c: Subroutines to implement command-line editing and history
   via readline and editline (libedit). */
/*-
 * Copyright (C) 2022 KomasanZura
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>

#include "ed.h"
#include "history.h"

#if defined WITH_READLINE || defined WITH_EDITLINE

#ifdef WITH_READLINE
#include <readline/readline.h>
#include <readline/history.h>
#endif /* WITH_READLINE */
#ifdef WITH_EDITLINE
#include <histedit.h>
#endif /* WITH_EDITLINE */

#ifdef WITH_READLINE
HISTORY_STATE *histories[3];
#endif /* WITH_READLINE */
#ifdef WITH_EDITLINE
EditLine *elhandle;
History *histories[3];
HistEvent histev;
char *edhist_prompt;
#endif /* WITH_EDITLINE */
char *histbuf = NULL, *histbufp = NULL;
int histbufsz = 0;
enum edhist_history_mode edhist_current;
int ldone = 1;
int edhist_active = 0;

#ifdef WITH_EDITLINE
void
edhist_setprompt(char *prompt)
{
	static char *empty = "";
	edhist_prompt = prompt ? prompt : empty;
}

char *
edhist_getprompt(EditLine *)
{
	return edhist_prompt;
}
#endif /* WITH_EDITLINE */

void
edhist_init(void)
{
	edhist_active = isatty(0);
	if (edhist_active) {
#ifdef WITH_READLINE
		rl_initialize();
		rl_erase_empty_line = 1;
		rl_bind_key('\t', rl_tab_insert);
		using_history();
		stifle_history(100);
		histories[EDHIST_MODE_GLOBAL] = history_get_history_state();
		stifle_history(100);
		histories[EDHIST_MODE_APPEND] = history_get_history_state();
		stifle_history(300);
		histories[EDHIST_MODE_COMMAND] = history_get_history_state();
		edhist_current = EDHIST_MODE_COMMAND;
#endif /* WITH_READLINE */
#ifdef WITH_EDITLINE
		elhandle = el_init("ed", stdin, stdout, stderr);
		histories[EDHIST_MODE_GLOBAL] = history_init();
		history(histories[EDHIST_MODE_GLOBAL], &histev, H_SETSIZE, 100);
		histories[EDHIST_MODE_APPEND] = history_init();
		history(histories[EDHIST_MODE_APPEND], &histev, H_SETSIZE, 100);
		histories[EDHIST_MODE_COMMAND] = history_init();
		history(histories[EDHIST_MODE_COMMAND], &histev, H_SETSIZE, 300);
		edhist_current = EDHIST_MODE_COMMAND;
		el_set(elhandle, EL_PROMPT, edhist_getprompt);
		el_set(elhandle, EL_HIST, history, histories[edhist_current]);
		el_set(elhandle, EL_SIGNAL, 1);
		el_source(elhandle, NULL);
#endif /* WITH_EDITLINE */
	}
}

void
edhist_reset(void)
{
	ldone = 1;
	histbufp = histbuf;
}

static void
edhist_commit(void)
{
	if (histbufp != histbuf && histbuf) {
		if (!(edhist_current == EDHIST_MODE_APPEND && histbuf[0] == '.' && histbuf[1] == 0)) {
#ifdef WITH_READLINE
			add_history(histbuf);
#endif /* WITH_READLINE */
#ifdef WITH_EDITLINE
			history(histories[edhist_current], &histev, H_ENTER, histbuf);
#endif /* WITH_EDITLINE */
		}
		histbufp = histbuf;
	}
}

static int
edhist_add(char *command, int app2prev)
{
	unsigned int n, oldlen;
	if (*command || app2prev) {
		n = strlen(command);
		oldlen = histbufp - histbuf;
		REALLOC(histbuf, histbufsz, oldlen + n + 2, 0);
		histbufp = histbuf + oldlen;
		if (app2prev) {
			*histbufp = '\n';
			histbufp++;
		}
		memcpy(histbufp, command, n + 1);
		histbufp += n;
	}
	return 1;
}

void
edhist_toggle_mode(enum edhist_history_mode newmode)
{
	if (edhist_current != newmode && !(edhist_current == EDHIST_MODE_GLOBAL && newmode == EDHIST_MODE_APPEND)) {
		edhist_commit();
#ifdef WITH_READLINE
		SPL1();
		free(histories[edhist_current]);
		histories[edhist_current] = history_get_history_state();
		SPL0();
		history_set_history_state(histories[newmode]);
#endif /* WITH_READLINE */
#ifdef WITH_EDITLINE
		el_set(elhandle, EL_HIST, history, histories[newmode]);
#endif /* WITH_EDITLINE */
		edhist_current = newmode;
	}
}

int
edhist_getc(char *prompt, int app2prev)
{
	static char *bigl = NULL, *biglp, *shortl;
#ifdef WITH_EDITLINE
	static int count;
	const char *templ;
#endif /* WITH_EDITLINE */
	char c;
	int add2hist = 0;

#ifdef WITH_EDITLINE
	edhist_setprompt(prompt);
#endif /* WITH_EDITLINE */
	if (!app2prev) {
		edhist_commit();
	}
	if (ldone) {
		if (bigl) {
			SPL1();
			free(bigl);
			bigl = NULL;
			SPL0();
		}
#ifdef WITH_READLINE
		bigl = readline(prompt);
		if (!bigl) {
			return EOF;
		}
		if (!*bigl) {
			if (prompt) {
				fprintf(stdout, "%s\n", prompt);
			} else {
				fputs("\n", stdout);
			}
			fflush(stdout);
		}
		ldone = 0;
		biglp = bigl;
		shortl = bigl;
	}
	c = *biglp;
	if (!c) {
		ldone = 1;
		c = '\n';
		add2hist = 1;
	} else {
		if (c == '\n') {
			*biglp = 0;
			add2hist = 1;
		}
		biglp++;
	}
	if (add2hist) {
		edhist_add(shortl, app2prev);
		shortl = biglp;
	}
#endif /* WITH_READLINE */
#ifdef WITH_EDITLINE
		templ = el_gets(elhandle, &count);
		if (!templ || count <= 0) {
			return EOF;
		}
		SPL1();
		bigl = strdup(templ);
		SPL0();
		ldone = 0;
		biglp = bigl;
		shortl = bigl;
	}
	c = *biglp;
	count--;
	if (!count) {
		ldone = 1;
		add2hist = 1;
	}
	if (c == '\n') {
		*biglp = 0;
		add2hist = 1;
	}
	if (add2hist) {
		edhist_add(shortl, app2prev);
	}
	if (count) {
		biglp++;
		if (add2hist) {
			shortl = biglp;
		}
	}
#endif /* WITH_EDITLINE */
	return c;
}

#endif /* defined WITH_READLINE || defined WITH_EDITLINE */
